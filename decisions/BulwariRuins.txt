

country_decisions = {
	rebuild_the_stone_palace = {
		major = yes
	
		potential = {
			owns_core_province = 688
			688 = {
				has_province_modifier = ruined_ekluzagnu
				NOT = { has_province_modifier = rebuilding_ekluzagnu }
			}
		}
		
		allow = {
			treasury = 350
		}
	
		effect = {
			add_treasury = -350
			country_event = {
				id = bulwar_flavour.26
			}
			clr_country_flag = rebuilding_ekluzagnu_fortress
			clr_country_flag = rebuilding_ekluzagnu_palace
		}
	}
}
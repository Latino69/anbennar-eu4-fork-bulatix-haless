# 
setup_vision = yes
government = republic
add_government_reform = noble_elite_reform
government_rank = 1
primary_culture = bahari
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
capital = 521
historical_rival = U14 #Mountainhuggers
historical_rival = F23 #Ovdal Tungr
historical_rival = U19 #Baharkand
historical_friend = F22 #Dartaxagerdim

1000.1.1 = {
	set_estate_privilege = estate_church_indepedent_clergy 
	add_isolationism = -2
	set_estate_privilege = estate_mages_organization_religious
}